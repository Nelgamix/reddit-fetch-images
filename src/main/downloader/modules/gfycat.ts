import {Observable} from "rxjs";
import {switchMap} from "rxjs/operators";
import {IItem} from "../../interfaces";
import {DownloaderModule} from "../downloader-module";

const pattern = /<source src="([0-9a-zA-Z\/.:]+)" type="video\/mp4"\/>/i;

/*
In the case of Gfycat, we need to make sure that the gif name
we get from the url respects the case.
If it does not, the conversion to the real file on their CDN
will return an error.
To work around this issue, we make a request to the page linked
by the url fetched from Reddit, and we search for
the gif name we got in the page. It should be somewhere in a
referenced as a link with the correct case.
We just make a regex with insensitive-case and get back
the correct gif name, with correct case. Bingo!
*/

export class Gfycat extends DownloaderModule {
    public name: string = "Gfycat";
    public matcher: RegExp = /^https?:\/\/(www\.)?gfycat\.com\/([a-z]*\/)?[a-zA-Z]+\/?(\?.*)?$/;

    public download(item: IItem): Observable<IItem> | null {
        return this.getUrl(item.url).pipe(
            switchMap((res: any) => {
                if (res.statusCode !== 200) {
                    throw new Error("Web page is not 200");
                }

                const url = this.regexpExec(pattern, res.body);
                const path = item.download.path;
                const ext = ".mp4";

                this.writeDownloadMetadata(item, url, path, ext);

                return this.finalizeDownload(item);
            }),
        );
    }
}
