import {EventEmitter} from "events";
import {IItem} from "../interfaces";
import {manager} from "./downloader-manager";

export class Downloader extends EventEmitter {
    private readonly items: IItem[];
    private readonly parallel: number;
    private queue: IItem[] = [];
    private downloaded: number = 0;

    constructor(items: IItem[], parallel: number) {
        super();

        this.items = items;
        this.parallel = parallel;

        this.queue.push(...items);
    }

    public start(): void {
        for (let i = 0; i < Math.min(this.parallel, this.items.length); i++) {
            this.launchNext();
        }
    }

    private launchNext() {
        if (this.queue.length > 0) {
            this.launchDownload(this.queue.splice(0, 1)[0]);
        }
    }

    private downloadSuccess(item: IItem) {
        this.emit("item-success", item);
        this.verifyEnd();
    }

    private downloadError(item: IItem, err: Error) {
        this.emit("item-error", item, err);
        this.verifyEnd();
    }

    private downloadNoDownloader(item: IItem) {
        this.emit("item-no-downloader", item);
        this.verifyEnd();
    }

    private verifyEnd() {
        this.downloaded++;
        if (this.queue.length === 0) {
            if (this.downloaded === this.items.length) {
                this.emit("end", this.items);
            }
        } else {
            this.launchNext();
        }
    }

    private launchDownload(item: IItem) {
        const downld = manager.startForItem(item);

        if (downld) {
            downld.subscribe((res: IItem) => {
                this.downloadSuccess(item);
            }, (error: Error) => {
                this.downloadError(item, error);
            });
        } else {
            this.downloadNoDownloader(item);
        }
    }
}
