import {Observable} from "rxjs";
import {switchMap} from "rxjs/operators";
import {IItem} from "../../interfaces";
import {DownloaderModule} from "../downloader-module";

const pattern = /<link rel="image_src" href="(.+)"\/>/i;

export class Imgur extends DownloaderModule {
    public name: string = "Imgur";
    public matcher: RegExp = /^https?:\/\/(([im])\.)?imgur\.com\/[0-9a-zA-Z_]+(\.(jpg|jpeg|png|gifv?)(\?.*)?)?$/;

    public download(item: IItem): Observable<IItem> | null {
        let url = this.removeParameters(item.url);
        const path = item.download.path;
        let ext = this.extractExtension(url);

        if (!ext) {
            // The extension does not match anything. It is probably a
            // link without the extension given.
            return this.getUrl(url).pipe(
                switchMap((res: any) => {
                    if (res.statusCode !== 200) {
                        throw new Error("Web page is not 200");
                    }

                    url = this.removeParameters(this.regexpExec(pattern, res.body));
                    ext = this.extractExtension(url);

                    this.writeDownloadMetadata(item, url, path, ext);

                    return this.finalizeDownload(item);
                }),
            );
        } else {
            /*
                The case of gifv is special. A gifv will
                be a simple gif transcoded in mp4 by imgur
                to save bandwidth and load time client side.
                So we correct the target by replacing gifv by
                mp4!
            */
            if (ext === ".gifv") {
                ext = ".mp4";
                url = url.substring(0, url.length - "gifv".length) + "mp4";
            }

            this.writeDownloadMetadata(item, url, path, ext);

            return this.finalizeDownload(item);
        }
    }
}
