# How to: launch

# Summary

To launch the script, you need to:

* Have a reddit account
* Get a developer access (app key and secret)
* Download and install NodeJS

# Get developer access

## Step 1

You need to go to the old reddit to register
a dev app.

![Step 1](./one.jpg)

## Step 2

Navigate to the preferences page, then go to the "apps" tab,
and click on "are you a developer? create an app..."

![Step 2](./two.jpg)

## Step 3

Now you need to add the details on the app.

The app can be named freely, but you must
select "script". You can also leave description empty,
and input any url on about url and redirect uri.

![Step 3](./three.jpg)

## Step 4

Finally, you can view the app key and the secret in the
panel displayed. Copy them to the appropriate location
in the .env file.

![Step 4](./four.jpg)

# Install Node

Visit https://nodejs.org/en/, download and install nodejs.