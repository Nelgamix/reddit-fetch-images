import request from "request";
import {Observable} from "rxjs";
import {IItem} from "../../interfaces";
import {DownloaderModule} from "../downloader-module";

const qualities = [
    "DASH_4_8_M",
    "DASH_2_4_M",
    "DASH_1_2_M",
    "DASH_600_K",
    "DASH_1080",
    "DASH_720",
    "DASH_480",
    "DASH_360",
    "DASH_240",
];

export class RedditVideo extends DownloaderModule {
    public matcher: RegExp = /^https?:\/\/v\.redd\.it\/[0-9a-z]+$/;
    public name: string = "Reddit Video";

    public download(item: IItem): Observable<any> | null {
        this.writeDownloadMetadata(item, undefined, item.download.path, ".mp4");

        return new Observable<IItem>((obs) => {
            this.tryWithQuality(item, obs, 0);
        });
    }

    private tryWithQuality(item: IItem, obs: any, quality: number) {
        if (quality >= qualities.length) {
            obs.error("No quality was found for this reddit video");
            obs.complete();
            return;
        }

        const qualityString = qualities[quality];
        const url = item.url + "/" + qualityString;

        request(url, {encoding: null}, (err, res, body) => {
            if (err) {
                obs.error(item);
                obs.complete();
                return;
            }

            if (res.statusCode === 200 && res.headers["content-type"] === "video/mp4") {
                this.writeDownloadMetadata(item, url, undefined, undefined, {quality: qualityString});

                this.finalizeDownload(item, body).subscribe(() => {
                    obs.next(item);
                    obs.complete();
                });

                return;
            }

            this.tryWithQuality(item, obs, quality + 1);
        });
    }
}
