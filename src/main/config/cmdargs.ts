import {ArgumentParser} from "argparse";

const parser = new ArgumentParser({
    version: "1.0.0",
    addHelp: true,
    description: "Reddit posts downloader",
});

parser.addArgument(
    [ "-e", "--env" ],
    {
        help: "specify the location to the .env file to load",
    },
);

parser.addArgument(
    [ "-c", "--conf" ],
    {
        help: "specify the location to the config.json file to load",
    },
);

parser.addArgument(
    "--dev",
    {
        help: "activate dev mode",
        nargs: 0,
    },
);

interface IArgs {
    dev: string[] | null;
    conf: string[] | null;
    env: string[] | null;
}

export function parseArgs(): IArgs {
    return parser.parseArgs();
}
