import {Observable} from "rxjs";
import {IItem} from "../interfaces";
import {DownloaderModule} from "./downloader-module";
import {DefaultImage} from "./modules/default-image";
import {DefaultVideo} from "./modules/default-video";
import {Gfycat} from "./modules/gfycat";
import {Imgur} from "./modules/imgur";
import {RedditImage} from "./modules/reddit-image";
import {RedditText} from "./modules/reddit-text";
import {RedditVideo} from "./modules/reddit-video";

export class DownloaderManager {
    private downloaders: DownloaderModule[] = [];

    public startForItem(item: IItem): Observable<any> | null {
        for (const downloader of this.downloaders) {
            if (downloader.matcher.test(item.url)) {
                const obs = downloader.download(item);
                if (obs) {
                    item.download.date = new Date().toISOString();
                    item.download.downloader = downloader.name;

                    return obs;
                }
            }
        }

        return null;
    }

    public registerDownloader(d: DownloaderModule) {
        this.downloaders.push(d);
    }

    public registerDownloaders(d: DownloaderModule[]) {
        for (const di of d) {
            this.registerDownloader(di);
        }
    }
}

export const manager = new DownloaderManager();

manager.registerDownloaders([
    new RedditImage(),
    new RedditVideo(),
    new Imgur(),
    new Gfycat(),
    new RedditText(),
    // Default downloaders comes last (otherwise they will
    // match over specific downloaders)
    new DefaultImage(),
    new DefaultVideo(),
]);
