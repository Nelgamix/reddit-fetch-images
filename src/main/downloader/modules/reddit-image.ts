import {Observable} from "rxjs";
import {IItem} from "../../interfaces";
import {DownloaderModule} from "../downloader-module";

export class RedditImage extends DownloaderModule {
    public matcher: RegExp = /^https?:\/\/i\.redd\.it\/[0-9a-z]+\.(jpg|jpeg|png|gifv?)$/;
    public name: string = "Reddit Image";

    public download(item: IItem): Observable<any> | null {
        const url = this.removeParameters(item.url);
        const path = item.download.path;
        const ext = this.extractExtension(url);

        this.writeDownloadMetadata(item, url, path, ext);

        return this.finalizeDownload(item);
    }
}
