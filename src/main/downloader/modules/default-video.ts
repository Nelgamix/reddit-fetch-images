import {Observable} from "rxjs";
import {IItem} from "../../interfaces";
import {DownloaderModule} from "../downloader-module";

export class DefaultVideo extends DownloaderModule {
    public name: string = "Default Video";
    public matcher: RegExp = /^.+\.(mp4|mkv|webm|flv)(\?.*)$/;

    public download(item: IItem): Observable<IItem> | null {
        const url = this.removeParameters(item.url);
        const path = item.download.path;
        const ext = this.extractExtension(url);

        this.writeDownloadMetadata(item, url, path, ext);

        return this.finalizeDownload(item);
    }
}
