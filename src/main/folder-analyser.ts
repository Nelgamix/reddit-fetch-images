import fs from "fs";
import moment from "moment";
import rimraf from "rimraf";

import {manager} from "./data-source/data-source-manager";
import * as logger from "./logger";
import * as utils from "./utils";

import {Observable, of} from "rxjs";
import {DataSourceModule} from "./data-source/data-source-module";
import {Downloader} from "./downloader/downloader";
import {IConfigFolder, IConfiguration, IIndex, IItem} from "./interfaces";

const prettyBytes = require("pretty-bytes");
const fsUtils = require("nodejs-fs-utils");

export class FolderAnalyser {
    private index: IIndex = {success: [], error: [], noDownloader: []};
    private indexSession: IIndex = {success: [], error: [], noDownloader: []};
    private readonly path: string;
    private readonly indexName: string;
    private readonly config: IConfigFolder;
    private readonly globalConfig: IConfiguration;
    private readonly dataSource: DataSourceModule;

    constructor(config: IConfigFolder, globalConfig: IConfiguration, indexName?: string) {
        this.config = config;
        this.globalConfig = globalConfig;

        this.path = globalConfig.path + "/" + this.config.path;
        this.indexName = indexName || "00000__index.json";

        const ds = manager.identifySource(config.type, config, this.index, globalConfig);
        if (!ds) {
            throw new Error("No data source for " + config.type);
        }

        this.dataSource = ds;
    }

    public run(): Observable<IIndex> {
        return new Observable((obs) => {
            logger.console.writeln("\n ***** ...Launching... *****");
            logger.console.writeln(" * Type: " + this.config.type);
            logger.console.writeln(" * Path: " + this.config.path);
            const p1 = moment();
            logger.console.writeln("\n -- Phase 1 -- Folder analysis");
            this.analyse();

            const p2 = moment();
            logger.console.writeln(" * Time taken: " + this.getDurationSecs(p1, p2) + " seconds");
            logger.console.writeln("\n -- Phase 2 -- Get items to download");
            this.getData().subscribe((items: IItem[]) => {
                logger.console.writeln("Number of items fetched: " + items.length);
                const p3 = moment();
                logger.console.writeln(" * Time taken: " + this.getDurationSecs(p2, p3) + " seconds");
                logger.console.writeln("\n -- Phase 3 -- Analyse items");
                const finalItems = this.analyseItems(items);
                logger.console.writeln("Total items to download: " + finalItems.length);

                const p4 = moment();
                logger.console.writeln(" * Time taken: " + this.getDurationSecs(p3, p4) + " seconds");

                if (finalItems.length === 0) {
                    logger.console.writeln("No items to download were found.");
                    logger.console.writeln("\n -- Phase 4 -- Recap");
                    this.printRecap(p1, undefined, p4);
                    obs.next(this.index);
                    obs.complete();
                    return;
                }

                logger.console.writeln("\n -- Phase 4 -- Download items");
                this.downloadItems(finalItems).subscribe(() => {
                    const p5 = moment();
                    const dTime = this.getDurationSecs(p4, p5);
                    logger.console.writeln(" * Time taken: " + dTime + " seconds");
                    logger.console.writeln("\n -- Phase 5 -- Write index");
                    this.saveIndex();
                    const pEnd = moment();
                    logger.console.writeln(" * Time taken: " + this.getDurationSecs(p5, pEnd) + " seconds");

                    logger.console.writeln("\n -- Phase 6 -- Recap");
                    this.printRecap(p1, dTime, pEnd);

                    obs.next(this.index);
                    obs.complete();
                });
            });
        });
    }

    private analyse(): void {
        const wipe = this.config.wipe;

        if (utils.mkdirRecursive(this.path)) {
            logger.console.writeln(this.path + " was created");
        } else if (wipe) {
            rimraf.sync(this.path + "/*");
            logger.console.writeln(this.path + " was wiped (config is on)");
        } else {
            this.setIndex(this.openIndexFile());
        }

        this.analyseOptions();
    }

    private getData(): Observable<IItem[]> {
        const gd = this.dataSource.getData();
        return gd ? gd : of([]);
    }

    private analyseItems(items: IItem[]): IItem[] {
        items.forEach((item: IItem) => item.download.path = this.path);
        return this.dataSource.analyseItems(items) || [];
    }

    private downloadItems(items: IItem[]): Observable<void> {
        return new Observable((obs) => {
            let i = 0;
            const total = items.length;
            const downloader = new Downloader(items, this.globalConfig.options.parallel);
            downloader.start();

            downloader.on("end", () => {
                logger.console.writeln("Download is finished.");
                obs.next();
                obs.complete();
            });

            downloader.on("item-success", (item: IItem) => {
                this.index.success.push(item);
                this.indexSession.success.push(item);
                this.printItem("OK", item, ++i, total);
            });

            downloader.on("item-error", (item: IItem, err: Error) => {
                item.download.error = err.message;
                this.index.error.push(item);
                this.indexSession.error.push(item);
                utils.deleteFileIfExists(item.download.path);
                this.printItem("ER", item, ++i, total, err.message);
            });

            downloader.on("item-no-downloader", (item: IItem) => {
                this.index.noDownloader.push(item);
                this.indexSession.noDownloader.push(item);
                this.printItem("ND", item, ++i, total);
            });
        });
    }

    private printItem(success: string, item: IItem, i: number, total: number, error?: string): void {
        logger.console.writeln(
            ` ${success} `.padStart(4) +
            ` ${i} / ${total} `.padStart(12) +
            ` ${item.name} `.padStart(12) +
            (success === "OK" ? ` ${item.download.path} ` : "") +
            (error ? ` ${error} ` : ""),
        );
    }

    private printRecap(startTime: moment.Moment, dlTime: number | undefined, endTime: moment.Moment): void {
        const fSize = this.getFolderSize();
        logger.console.writeln(" * Folder size: " + prettyBytes(fSize));

        // Broken when because we don't get the size of what we download be what is inside of the folder
        // Shows wrong value if we complete the folder instead of filling it from scratch
        // Solution: only count size of files downloaded
        /*if (dlTime) {
            const avgSize = prettyBytes(Math.ceil(this.getAvgSpeed(fSize, dlTime)));
            logger.console.writeln(" * Average speed: " + avgSize + "/s");
        }*/

        if (this.indexSession.success.length > 0) {
            logger.console.writeln(" * -- OK: " + this.indexSession.success.length);
        }

        if (this.indexSession.error.length > 0) {
            logger.console.writeln(" * -- Error: " + this.indexSession.error.length);
        }

        if (this.indexSession.noDownloader.length > 0) {
            logger.console.writeln(" * -- No Downloader: " + this.indexSession.noDownloader.length);
        }

        logger.console.writeln(" * Time taken (total): " + this.getDurationSecs(startTime, endTime) + " seconds");
    }

    private saveIndex(): void {
        this.index.success.sort((a: IItem, b: IItem) => a.download.number - b.download.number);
        this.index.error.sort((a: IItem, b: IItem) => a.download.number - b.download.number);
        this.index.noDownloader.sort((a: IItem, b: IItem) => a.download.number - b.download.number);

        const path = `${this.path}/${this.indexName}`;
        fs.writeFileSync(path, JSON.stringify(this.index, null, 4), {encoding: "utf-8"});
    }

    private setIndex(index: IIndex) {
        this.index = index;
        this.dataSource.setIndex(index);
    }

    private getFolderSize(): number {
        return fsUtils.fsizeSync(this.path);
    }

    private getAvgSpeed(size: number, secs: number): number {
        return size / secs;
    }

    private getDurationSecs(start: moment.Moment, end: moment.Moment): number {
        return moment.duration(end.diff(start)).asSeconds();
    }

    private openIndexFile(): IIndex {
        const indexPath = this.path + "/" + this.indexName;
        if (fs.existsSync(indexPath)) {
            logger.console.writeln(indexPath + " was read.");
            return JSON.parse(fs.readFileSync(indexPath, {encoding: "utf-8"}));
        } else {
            logger.console.writeln(indexPath + " does not exists.");
            return {success: [], error: [], noDownloader: []};
        }
    }

    private analyseOptions(): void {
        this.dataSource.analyseOptions(this.path);
    }
}
