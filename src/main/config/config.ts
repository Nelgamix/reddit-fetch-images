import dotenv from "dotenv";
import fs from "fs";
import {IConfigurationItem, VariableType} from "../interfaces";
import * as logger from "../logger";
import * as utils from "../utils";
import {confConfigurationItems, envConfigurationItems, folderConfigurationItems} from "./config-items";

export class ConfigurationParser {
    public config: any = {};

    private readonly envObject: any;
    private readonly configObject: any;

    constructor(envPath?: string, configPath?: string, dev?: boolean) {
        const envFinalPath = envPath || (!dev ? ".env" : ".env.dev");
        const configFinalPath = configPath || (!dev ? "configs/config.json" : "configs/config.dev.json");

        const sEnv = fs.readFileSync(envFinalPath, {encoding: "utf-8"});
        const sConfig = fs.readFileSync(configFinalPath, {encoding: "utf-8"});

        this.envObject = dotenv.parse(sEnv);
        this.configObject = JSON.parse(sConfig);

        for (const k in this.envObject) {
            process.env[k] = this.envObject[k];
        }

        this.config.dev = dev || false;

        for (const c of envConfigurationItems) {
            c.sourceContext = process.env;
            c.destContext = this.config;

            const val = this.getVal(c);
            this.setDst(c, val);
        }

        for (const c of confConfigurationItems) {
            c.sourceContext = this.configObject;
            c.destContext = this.config;

            const val = this.getVal(c);
            this.setDst(c, val);
        }

        this.config.folders = [];
        if (this.configObject.folders) {
            for (const f of this.configObject.folders) {
                const no = {};
                this.config.folders.push(no);
                for (const c of folderConfigurationItems) {
                    c.sourceContext = f;
                    c.destContext = no;

                    const val = this.getVal(c);
                    this.setDst(c, val);
                }
            }
        }
    }

    public dump(): void {
        logger.bunyan.info(this.config);
    }

    private getVal(config: IConfigurationItem) {
        switch (config.type) {
            case VariableType.STRING:
                return this.getStringVal(config);
            case VariableType.NUMBER:
                return this.getNumberVal(config);
            case VariableType.BOOLEAN:
                return this.getBooleanVal(config);
            case VariableType.OBJECT:
                return this.getObjectVal(config);
        }
    }

    private getRaw(config: IConfigurationItem,
                   typeNeeded: string,
                   valValidator?: (v: any) => boolean,
                   transform?: (v: any) => any) {
        let val = this.getSrc(config);
        const def = config.defaultValue;

        if (transform && val !== undefined && typeof val !== typeNeeded) {
            val = transform(val);
        }

        const acceptVal = val !== undefined
            && typeof val === typeNeeded
            && (!valValidator || valValidator(val))
            && (!config.validator || config.validator(val));

        if (!acceptVal) {
            if (def === undefined) {
                const message = "The value for " +
                    config.name +
                    " was not specified (or invalid) and no default value was given";
                throw new Error(message);
            }

            if (typeof def === "function") {
                return def(config.sourceContext);
            }

            return def;
        }

        return val;
    }

    private getStringVal(config: IConfigurationItem) {
        return this.getRaw(config, "string", undefined, undefined);
    }

    private getNumberVal(config: IConfigurationItem) {
        return this.getRaw(config, "number", (val) => !isNaN(val), (val) => parseInt(val, 10));
    }

    private getBooleanVal(config: IConfigurationItem) {
        return this.getRaw(config, "boolean", undefined, (val) => val === "true");
    }

    private getObjectVal(config: IConfigurationItem) {
        return this.getRaw(config, "object", undefined, undefined);
    }

    private getSrc(config: IConfigurationItem) {
        return utils.navigateInObject(config.sourceContext, config.srcPath);
    }

    private setDst(config: IConfigurationItem, val: any) {
        const path = config.dstPath || config.srcPath;
        const arPath = path.split(".");
        let dst;
        const target = arPath[arPath.length - 1];
        if (arPath.length > 1) {
            const rPath = arPath.slice(0, arPath.length - 1).join(".");
            dst = utils.navigateInObject(config.destContext, rPath, undefined, true);
        } else {
            dst = config.destContext;
        }

        dst[target] = val;
    }
}
