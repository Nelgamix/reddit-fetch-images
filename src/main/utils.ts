import fs from "fs";

export function mkdirRecursive(path: string) {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path, {recursive: true});
        return true;
    }

    return false;
}

export function deleteFileIfExists(path: string): boolean {
    if (fs.existsSync(path)) {
        fs.unlinkSync(path);
        return true;
    }

    return false;
}

export function objectIsEmpty(obj: any): boolean {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
}

export function navigateInObject(obj: any, path: string, splitSymbol?: string, create?: boolean): any {
    const tokens = path ? path.split(splitSymbol || ".") : [];
    let res = obj;

    for (const token of tokens) {
        if (res.hasOwnProperty(token)) {
            res = res[token];
        } else if (create) {
            res[token] = {};
            res = res[token];
        } else {
            return undefined;
        }
    }

    return res;
}
