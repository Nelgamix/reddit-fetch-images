import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import Snoowrap from "snoowrap";
import {IConfigFolder, IConfiguration, IIndex, IItem, IRedditConfiguration, IRedditOptions} from "../../interfaces";
import * as logger from "../../logger";
import * as utils from "../../utils";
import {DataSourceModule} from "../data-source-module";

export const redditSource = {
    name: "reddit",
    instance: (config: IConfigFolder, index: IIndex, gc: IConfiguration) => new RedditSource(config, index, gc.reddit),
};

/**
 * Reddit source.
 * Options: {
 *  hide: {nsfw: boolean}
 *  group: {nsfw: boolean}
 * }
 */
export class RedditSource extends DataSourceModule {
    public name: string = "Reddit";

    private readonly api: Snoowrap;

    constructor(config: IConfigFolder, index: IIndex, redditConfiguration: IRedditConfiguration) {
        super(config, index);

        this.api = new Snoowrap(redditConfiguration);
    }

    public getData(): Observable<IItem[]> | undefined {
        const options = this.getOptions();
        const typeTokens = this.config.type.split(":");
        const typeSpec = typeTokens[1];

        switch (typeSpec) {
            case "saved":
                return this.getSaved(options).pipe(
                    map((items: any[]) => this.constructItems(items)),
                );
            case "upvoted":
                return this.getUpvoted(options).pipe(
                    map((items: any[]) => this.constructItems(items)),
                );
        }

        return undefined;
    }

    public analyseOptions(path: string): void {
        if (utils.navigateInObject(this.config.options, "group.nsfw")) {
            utils.mkdirRecursive(path + "/nsfw");
        }
    }

    public analyseItems(items: IItem[]): IItem[] {
        const fitems: IItem[] = [];

        if (this.config.retry.error && this.index.error.length > 0) {
            logger.console.writeln("Retrying " + this.index.error.length + " errored posts");
            this.index.error.forEach((item: IItem) => this.getDownloadMetadata(item.download.number));
            fitems.push(...this.index.error);
            this.index.error = [];
        }

        if (this.config.retry.noDownloader && this.index.noDownloader.length > 0) {
            logger.console.writeln("Retrying " + this.index.noDownloader.length + " posts without a downloader");
            this.index.noDownloader.forEach((item: IItem) => this.getDownloadMetadata(item.download.number));
            fitems.push(...this.index.noDownloader);
            this.index.noDownloader = [];
        }

        if (items.length > 0) {
            let i = 1;
            const lastItem = this.getLastSuccessItem();
            if (lastItem) {
                i += lastItem.download.number;
                logger.console.writeln("Will start to save new posts from " + i);
            }

            for (const item of items.reverse()) {
                item.download.number = i++;
                if (utils.navigateInObject(this.config.options, "group.nsfw") && item.metadata.nsfw) {
                    item.download.path += "/nsfw";
                }
                item.download.path += "/" + String(item.download.number).padStart(5, "0") + "__" + item.name;
                fitems.push(item);
            }
        } else {
            logger.console.writeln("No new posts to download");
        }

        return fitems;
    }

    private getOptions(): IRedditOptions | undefined {
        const options: IRedditOptions = {};

        if (this.config.options.max) options.limit = this.config.options.max;
        if (this.config.options.before) options.before = this.config.options.before;
        if (this.config.options.after) options.after = this.config.options.after;
        const lastItem = this.getLastSuccessItem();
        if (lastItem) options.before = lastItem.name;

        return !utils.objectIsEmpty(options) ? options : undefined;
    }

    private constructItems(objs: any[]): IItem[] {
        let ret = objs;

        if (utils.navigateInObject(this.config.options, "hide.nsfw")) {
            ret = ret.filter((item: any) => item.over_18 === false);
        }

        ret = ret.map((item: any) => {
            return {
                id: item.id,
                url: item.url,
                name: item.name,
                download: this.getDownloadMetadata(undefined, this.name),
                metadata: {
                    id: item.id,
                    url: item.url,
                    name: item.name,
                    subreddit: item.subreddit_id,
                    subredditName: item.subreddit.display_name,
                    subredditSubscribers: item.subreddit_subscribers,
                    domain: item.domain,
                    author: item.author_fullname,
                    authorName: item.author.name,
                    title: item.title,
                    text: item.selftext,
                    score: item.score,
                    postHint: item.post_hint,
                    nsfw: item.over_18,
                    permalink: item.permalink,
                    createdUtc: item.created_utc,
                },
            };
        });

        return ret;
    }

    private getUpvoted(opt?: IRedditOptions): Observable<any[]> {
        return opt && opt.limit
            ? this.getObs(this.api.getMe().getUpvotedContent(opt))
            : this.getObs(this.api.getMe().getUpvotedContent(opt).fetchAll());
    }

    private getSaved(opt?: IRedditOptions): Observable<any[]> {
        return opt && opt.limit
            ? this.getObs(this.api.getMe().getSavedContent(opt))
            : this.getObs(this.api.getMe().getSavedContent(opt).fetchAll());
    }
}
