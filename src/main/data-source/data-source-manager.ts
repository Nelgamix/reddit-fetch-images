import {IConfigFolder, IConfiguration, IDataSourceModule, IIndex} from "../interfaces";
import {DataSourceModule} from "./data-source-module";
import {redditSource} from "./modules/reddit";

class DataSourceManager {
    private readonly sources: IDataSourceModule[] = [];

    public registerSource(s: IDataSourceModule) {
        this.sources.push(s);
    }

    public registerSources(s: IDataSourceModule[]) {
        for (const sc of s) {
            this.registerSource(sc);
        }
    }

    public identifySource(type: string, c: IConfigFolder, i: IIndex, gc: IConfiguration): DataSourceModule | undefined {
        const name = type.split(":")[0];
        if (!name) {
            return undefined;
        }

        for (const s of this.sources) {
            if (s.name === name) {
                return s.instance(c, i, gc);
            }
        }
    }
}

export const manager = new DataSourceManager();

manager.registerSources([
    redditSource,
]);
