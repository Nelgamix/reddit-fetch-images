import {parseArgs} from "./config/cmdargs";
import {ConfigurationParser} from "./config/config";
import {Launcher} from "./launcher";
import * as logger from "./logger";

logger.bunyan.info("Start");

const args = parseArgs();
const env = args.env !== null ? args.env[0] : undefined;
const conf = args.conf !== null ? args.conf[0] : undefined;
const dev = args.dev !== null;

const configParser = new ConfigurationParser(env, conf, dev);

if (dev) {
    configParser.dump();
}

const launcher = new Launcher(configParser.config);
launcher.launch().subscribe(() => 0, () => 0, () => logger.bunyan.info("Finished"));
