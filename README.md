# Reddit fetch images

This program will fetch and save on the disk the content from the
posts you upvoted/saved. It works for the most popular content providers
(e.g. reddit, imgur, gfycat, ...) and will automatically fetch the best
quality for images/videos and resolve issues by itself when possible.

Launching the script from a folder where you already have posts downloaded
will simply identify the last post downloaded and will fetch and download
the posts upvoted after this one.

Of course, because it uses the reddit API, it will NOT make magic
happen: posts upvoted/saved more than 6 months from their creation
(that are archived) will not appear in your upvoted posts, so they will
not be downloaded.
Also, there seems to be a limit of posts we can fetch from the API
(maybe around 1000).
The better is to regularly launch the script so the latest posts
are downloaded.

# Limitations

* Need a developer API key
* Can only download the posts upvoted/saved from your own account
* Can only download the last 1000 posts you upvoted/saved, and
posts that you have upvoted max 6 month from now.
* Videos downloaded will probably not have sound (since some websites
give two streams for the video: one for video, one for audio,
and the only way to merge them together is by using ffmpeg and
installing it is painful).

# Prerequisites

1. Reddit API key/secret. You need to go to https://www.reddit.com/prefs/apps
and create a new app. (follow this tutorial: https://github.com/reddit-archive/reddit/wiki/OAuth2)
2. Reddit account.
3. Node.js: https://nodejs.org/en/

# How to install and run

1. Fill .env.example with your credentials (reddit username and password),
with the user agent of your choice (go to https://www.whoishostingthis.com/tools/user-agent/
to find out, and copy it to USER_AGENT) and with your API keys
2. Rename .env.example to .env
3. Run `npm install` in command line.
4. Run `npm run launch` in command line.
5. Wait until finished downloading.
6. Go to ./save to find the images downloaded.

# Development

## Resources

* [Reddit API](https://www.reddit.com/dev/api#GET_user_{username}_upvoted)
* [Snoowrap](https://not-an-aardvark.github.io/snoowrap/RedditUser.html#getUpvotedContent__anchor)

## Todo

* Downloader for Imgur albums
* UI (need electron)?
* Duplicate image detection (need a detector)?
* Download audio with video files and mix into the file (need ffmpeg)?
* Add possibility to sort images in folders based on their subreddit
* Add possibility to filter upvoted posts (by subreddit, NSFW or not, ...)
