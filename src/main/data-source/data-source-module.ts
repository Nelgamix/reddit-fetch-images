import {Observable} from "rxjs";
import {IConfigFolder, IDownloadMetadata, IIndex, IItem} from "../interfaces";

export abstract class DataSourceModule {
    public abstract name: string;

    protected constructor(protected config: IConfigFolder, protected index: IIndex) {}

    public abstract getData(): Observable<IItem[]> | undefined;
    public abstract analyseOptions(path: string): void;
    public abstract analyseItems(items: IItem[]): IItem[];

    public setIndex(index: IIndex) {
        this.index = index;
    }

    protected getObs(o: any): Observable<any[]> {
        return new Observable((obs) => {
            o.then((res: any) => {
                obs.next(res);
                obs.complete();
            });
        });
    }

    protected getLastSuccessItem(): IItem | undefined {
        return this.index.success.length > 0 ? this.index.success[this.index.success.length - 1] : undefined;
    }

    protected getDownloadMetadata(num?: number, source?: string): IDownloadMetadata {
        return {
            number: num === undefined ? 0 : num,
            path: "",
            url: "",
            extension: "",
            source: source || "",
            downloader: "",
            date: "",
        };
    }
}
