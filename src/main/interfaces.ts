import {DataSourceModule} from "./data-source/data-source-module";

export interface IIndex {
    success: IItem[];
    error: IItem[];
    noDownloader: IItem[];
}

export interface IDownloadMetadata {
    number: number;
    path: string;
    url: string;
    extension: string;
    source: string;
    downloader: string;
    date: string;
    notes?: any;
    error?: string;
}

export interface IRedditMetadata {
    id: string;
    url: string;
    name: string;
    subreddit: string;
    subredditName: string;
    subredditSubscribers: number;
    domain: string;
    author: string;
    authorName: string;
    title: string;
    text: string;
    score: number;
    postHint: string;
    nsfw: boolean;
    permalink: string;
    createdUtc: number;
}

export interface IItem {
    id: string;
    url: string;
    name: string;

    download: IDownloadMetadata;
    metadata: any;
}

export enum VariableType {
    STRING,
    NUMBER,
    BOOLEAN,
    OBJECT,
}

export interface IRedditConfiguration {
    userAgent: string;
    clientId: string;
    clientSecret: string;
    username: string;
    password: string;
}

export interface IConfigFolderRetry {
    error: boolean;
    noDownloader: boolean;
}

export interface IConfigFolder {
    type: string;
    path: string;
    wipe: boolean;
    retry: IConfigFolderRetry;
    options: any;
}

export interface IConfigOptions {
    parallel: number;
}

export interface IConfiguration {
    reddit: IRedditConfiguration;
    path: string;
    folders: IConfigFolder[];
    options: IConfigOptions;
}

export interface IConfigurationItem {
    name: string;
    description?: string;
    type: VariableType; // the type of variable needed
    srcPath: string; // srcPath is the path where to find the var in the src object
    dstPath?: string; // dstPath is the path where to store the object in the config object
    defaultValue?: any; // undefined says no default value; will throw an error if user did not input it
    validator?: (value: any) => boolean; // undefined says no validator; all values will be valid

    // Programmatic added
    sourceContext?: any;
    destContext?: any;
}

export interface IRedditOptions {
    limit?: number;
    after?: string;
    before?: string;
}

export interface IDataSourceModule {
    name: string;
    instance: (c: IConfigFolder, i: IIndex, gc: IConfiguration) => DataSourceModule;
}
