import fs from "fs";
import {Observable} from "rxjs";
import wrap = require("word-wrap");
import {IItem} from "../../interfaces";
import {DownloaderModule} from "../downloader-module";

export class RedditText extends DownloaderModule {
    public matcher: RegExp = /^https?:\/\/www\.reddit\.com\/r\/[0-9a-zA-Z]+\/comments\/.+$/;
    public name: string = "Text";

    public download(item: IItem): Observable<any> | null {
        const url = item.url;
        const path = item.download.path;
        const ext = ".txt";

        this.writeDownloadMetadata(item, url, path, ext);

        let data = item.metadata.title;
        data += "\n\n\n\n";
        data += item.metadata.text;
        data = wrap(data);

        return new Observable<IItem>((obs) => {
            fs.writeFile(item.download.path, data, (err) => {
                if (err) {
                    obs.error(err);
                    obs.complete();
                    return;
                }

                obs.next(item);
                obs.complete();
            });
        });
    }
}
