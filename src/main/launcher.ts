import {concat, Observable, of} from "rxjs";
import {FolderAnalyser} from "./folder-analyser";
import {IConfiguration, IIndex} from "./interfaces";

export class Launcher {
    private readonly config: IConfiguration;

    constructor(config: IConfiguration) {
        this.config = config;
    }

    public launch(): Observable<IIndex | undefined> {
        const queue: Array<Observable<IIndex>> = [];

        for (const folder of this.config.folders) {
            const fa = new FolderAnalyser(folder, this.config);
            queue.push(fa.run());
        }

        return queue.length > 0 ? concat(...queue) : of(undefined);
    }
}
