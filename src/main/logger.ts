import * as logger from "bunyan";
import fs from "fs";
import * as utils from "./utils";

utils.mkdirRecursive("./logs");

class ConsoleLogger {
    public write(item: string) {
        process.stdout.write(item);
    }

    public writeln(item: string) {
        this.write(item + "\n");
    }

    public writejson(item: any) {
        this.write(JSON.stringify(item, null, 4));
    }
}

export const console = new ConsoleLogger();
export const bunyan = logger.createLogger({name: "console"});
export const file = logger.createLogger({name: "console", stream: fs.createWriteStream("./logs/log.json")});
