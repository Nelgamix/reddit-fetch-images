import axios from "axios";
import fs from "fs";
import fspath from "path";
const httpAdapter = require("axios/lib/adapters/http");
import {from, Observable} from "rxjs";
import {mapTo} from "rxjs/operators";
import {IItem} from "../interfaces";

export abstract class DownloaderModule {
    public abstract matcher: RegExp;
    public abstract name: string;

    public abstract download(item: IItem): Observable<any> | null;

    protected extractExtension(paths: string): string {
        return fspath.extname(paths);
    }

    protected extractLastPath(url: string): string {
        const paths = url.split("/");
        return paths[paths.length - 1];
    }

    protected removeParameters(url: string): string {
        const indexOfQM = url.indexOf("?");
        if (indexOfQM > 0) {
            return url.substring(0, indexOfQM);
        } else {
            return url;
        }
    }

    protected regexpExec(regexp: RegExp, s: string): string {
        const groups = regexp.exec(s);
        if (!groups || groups.length < 2) {
            throw new Error("Regexp did not match anything");
        }

        return groups[1];
    }

    protected finalizeDownload(item: IItem, streamData?: any): Observable<IItem> {
        if (!streamData) {
            return this.getData(item.download.url, item.download.path).pipe(
                mapTo(item),
            );
        } else {
            return this.writeData(item.download.path, streamData).pipe(
                mapTo(item),
            );
        }
    }

    protected writeDownloadMetadata(item: IItem,
                                    url?: string,
                                    path?: string,
                                    extension?: string,
                                    notes?: any) {
        if (url) item.download.url = url;
        if (path) item.download.path = path;
        if (extension) item.download.extension = extension;
        if (path && extension && !path.endsWith(extension)) item.download.path += extension;
        if (notes) item.download.notes = notes;
    }

    protected getData(url: string, dest: string): Observable<void> {
        return new Observable((obs) => {
            const output = fs.createWriteStream(dest);
            axios.get(url, {responseType: "stream", adapter: httpAdapter})
                .then((response) => {
                    const stream = response.data;
                    stream.on("data", (chunk: ArrayBuffer) => {
                        output.write(Buffer.from(chunk));
                    });
                    stream.on("end", () => {
                        output.end();
                        obs.next();
                        obs.complete();
                    });
                });
            // request(url)
            //     .on("error", (err) => {
            //         obs.error(err);
            //         obs.complete();
            //     })
            //     .on("end", () => {
            //         obs.next();
            //         obs.complete();
            //     })
            //     .pipe(fs.createWriteStream(dest));
        });
    }

    protected getUrl(url: string): Observable<any> {
        return from(axios.get(url));
        // return new Observable((obs) => {
        //     request(url, (err, res, body) => {
        //         if (err) {
        //             obs.error(err);
        //             obs.complete();
        //             return;
        //         }
        //
        //         obs.next(res);
        //         obs.complete();
        //     });
        // });
    }

    protected writeData(path: string, data: any): Observable<void> {
        return new Observable((obs) => {
            const ws = fs.createWriteStream(path);

            ws.on("error", (err) => {
                obs.error(err);
                obs.complete();
            });

            ws.on("finish", () => {
                obs.next();
                obs.complete();
            });

            ws.write(data);

            ws.end();
            ws.close();
        });
    }
}
